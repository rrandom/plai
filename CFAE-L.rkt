#lang plai

;; FAE with expression closures and laziness


(define-type CFAE/L
  [num (n number?)]
  [add (lhs CFAE/L?)
       (rhs CFAE/L?)]
  [id (name symbol?)]
  [fun (param symbol?) (body CFAE/L?)]
  [app (fun-expr CFAE/L?) (arg CFAE/L?)])


(define-type CFAE/L-Value
  [numV (n number?)]
  [closureV (param symbol?)
            (body CFAE/L?)
            (env Env?)]
  [exprV (expr CFAE/L?)
         (env Env?)
         (cache boxed-boolean/CFAE/L-Value?)])


(define-type Env
  [mtSub]
  [aSub (name symbol?) (value CFAE/L-Value?) (env Env?)])


(define (boxed-boolean/CFAE/L-Value? v)
  (and (box? v)
       (or (boolean? (unbox v))
           (numV? (unbox v))
           (closureV? (unbox v)))))

(define (num+ n1 n2)
  (numV (+ (numV-n (strict n1)) (numV-n (strict n2)))))


(define (strict e)
  (type-case CFAE/L-Value e
    [exprV (expr env cache)
           (if (boolean? (unbox cache))
               (local [(define the-value (strict (interp expr env)))]
                 (begin 
                   (printf "Forcing exprV ~a to ~a ~n" expr the-value)
                   (set-box! cache the-value)
                   the-value))
               (begin
                 (printf "Using cached value ~n")
                 (unbox cache)))]
    [else e]))




(define (parse sexp)
  (cond
    [(number? sexp) (num sexp)]
    [(list? sexp)
     (case (first sexp)
       [(+) (add (parse (second sexp))
                 (parse (third sexp)))]
       [(fun) (fun (caadr sexp) (parse (caddr sexp)))]
       [else 
        (cond [(symbol? (first sexp))
               (app (first sexp) (parse (second sexp)))]
              [(and (list? (first sexp))
                    (symbol=? 'fun (caar sexp)))
               (app (parse (car sexp))
                    (parse (cadr sexp)))]
              [else (error "wrong fun")])])]
     [(symbol? sexp) (id sexp)]))


(define (interp expr env)
  (type-case CFAE/L expr
    [num (n) (numV n)]
    [add (l r) (num+ (interp l env) (interp r env))]
    [id (v) (lookup v env)]
    [fun (bound-id bound-body)
         (closureV bound-id bound-body env)]
    [app (fun-expr arg-expr)
         (local [(define fun-val (interp fun-expr env))
                 (define arg-val (exprV arg-expr env (box false)))]
           (interp (closureV-body fun-val)
                   (aSub (closureV-param fun-val)
                         arg-val
                         (closureV-env fun-val))))]))

(define (lookup name ds)
  (type-case Env ds
    [mtSub () (error 'lookup "no binding for identifier")]
    [aSub (bound-name bound-value rest-ds)
          (if (symbol=? name bound-name)
              bound-value
              (lookup name rest-ds))]))

(test (interp (parse '((fun (x) (+ x 4)) 5)) (mtSub))
      (numV 9))